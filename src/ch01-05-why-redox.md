為什麼需要Linux ？
==================

有很多操作系統。那為什麼我們應該建造一個新的作業系統。為現有項目做貢獻不是更好嗎。Redox 社區認為現有項目不足，我們的目標最好由從頭開始構建的新項目實現。我們來看看現有的3個專案。

### Linux

Linux 運行在世界各個地方，從高效能的伺服器到微型嵌入式裝置，皆有Linux啟動運作。事實上，許多 Redox 社區成員都將 Linux 作為他們的主要工作站。然而，Linux 並不是操作系統開發創新的理想平台。

- 無窮盡地遺留程式碼：無所不在的舊式的系統呼叫程式碼，好似永遠存著，為不會再購買的硬體所撰寫的驅動程式存在系統核心，而且還是必須的一部份。當它們不作用的時候，在核心空間運行它們是不需要的，而且可能成為系統崩潰、安全問題及無法預期的臭蟲的來源。
- 龐大的代碼庫：如果你想要為Linux做出貢獻，你必須找到一個足夠容納二千五百萬行程式碼的空間，這還只是核心程式碼。會造成這樣的狀況，是由於Linux是單體式架構。
- 苛求的公共許可證：Linux的授權是GPL2，不允許在核心中使用採用其它自由軟體授權的軟體。更多的資訊，請看我們的哲學
- Lack of memory safety: Linux has had numerous issues with memory safety throughout time. C is a fine language, but for such a security critical system, C is difficult to use safely.
- 缺乏記憶體安全性：Linux已經有許多的記憶體安全議題。C是一個好的程式語言，但是對於安全極度需求的系統，C很難安全地使用。

### BSD

It is no secret that we're more in favor of BSD. The BSD community has led the way in many innovations in the past 2 decades. Things like [jails][jails] and [ZFS][ZFS] yield more reliable systems, and other operating systems are still catching up.

That said, BSD doesn't meet our needs either:

- It still has a monolithic kernel. This means that a single buggy driver can crash, hang, or, in the worst case, cause damage to the system.
- The use of C in the kernel makes it probable to write code with memory safety issues.

### MINIX

And what about MINIX? Its microkernel design is a big influence on the Redox project, especially for reasons like [reliability][reliability]. MINIX is the most in line with Redox's philosophy. It has a similar design, and a similar license.

- Use of C - again, we would like drivers and the kernel to be written in Rust, to improve readability and organization, and to catch more potential safety errors. Compared to monolithic kernels, Minix is actually a very well-written and manageable code base, but it is still prone to memory unsafety bugs, for example. These classes of bugs can unfortunately be quite fatal, due to their unexpected nature.
- Lack of driver support - MINIX does not work well on real hardware, partly due to having less focus on real hardware.
- Less focus on "Everything is a File" - MINIX does focus less on "Everything is a File" than various other operating systems, like Plan9. We are particularly focused on this idiom, for creating a more uniform program infrastructure.

The Need for Something New
--------------------------

We have to admit, that we do like the idea of writing something that is our own (Not Invented Here syndrome). There are numerous places in the MINIX 3 source code where we would like to make changes, so many that perhaps a rewrite in Rust makes the most sense.

- Different VFS model, based on URLs, where a program can control an entire segmented filesystem
- Different driver model, where drivers interface with filesystems like `network:` and `audio:` to provide features
- Different file system, RedoxFS, with a [TFS][TFS] implementation in progress
- User space written mostly in Rust
- [Orbital][Orbital], a new GUI

[jails]: https://www.freebsd.org/doc/handbook/jails.html
[ZFS]: https://www.freebsd.org/doc/handbook/zfs.html
[reliability]: http://wiki.minix3.org/doku.php?id=www:documentation:reliability
[TFS]: https://gitlab.redox-os.org/redox-os/tfs
[Orbital]: https://gitlab.redox-os.org/redox-os/orbital
