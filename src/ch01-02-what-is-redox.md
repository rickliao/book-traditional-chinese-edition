# Redox是什麼？

Redox是一個針對各種應用，具廣泛性目地的作業系統，並且全部以系統開發語言Rust進行實作。我們的目的是在提供一個安全及自由的，全功能、Unix-like的微核心架構作業系統，而且是安全及免費的。。

我們對POSIX有良好適中的相容性，許多程式不需要修改即可執行。

我們得到來自於[Plan9], [Minix], [Linux], 及 [BSD]的鼓舞。Redox的目標在結合多年對於作業系統的研究及許多得之不易的經驗，將其具現化成一個現代且熟悉的作業系統。

在此同時，Redox支援：

* 所有 x86-64 CPUs
* 具備VBE支援的顯示卡。(所有過去十年以來的Nvidia, Inter, and AMD繪圖顯示卡)
* AHCI磁碟。
* E1000 或 RTL8168網路卡。
* Intel HDA音效控制器。
* PS/2滑鼠及鍵盤模擬。
*工作清單：持繼新增*

這本書分成幾部份：

- [第一章](./ch01-01-welcome.html) - 什麼是Redox
- [第二章](./ch02-01-getting-started.html) - 建置及執行Redox
- [第三章](./ch03-01-explore.html) - Redox的執行
- [第四章](./ch04-01-design.html) - Redox大局觀。
- [第五章](./ch05-01-user-space.html) - 開發Redox程式及套件。
- [第六章](./ch06-01-contributing.html) - 貢獻又與開發團隊交流。

閱讀這份文件，並不需要您有Rust或作業系統方面的先備知識。



